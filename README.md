# Head-Coupled AR

An app that attempts to combine AR from a devices rear-facing camera with head position data from the front-facing camera to create a 'portal effect' for a single user.

Create a 3D, textured scene from the rear-facing camera (difficult without infrared for depth) and move the camera in that scene to match user's angle

## Research:

https://www.youtube.com/watch?v=bBQQEcfkHoE


- TODO: find xray video made at studiorx
